bid.mdown
=========================

File này mô tả chi tiết về lượt đấu giá 

## Các thuộc tính
- bid_item : sản phẩm đấu giá
- bidder : người thực hiện lượt đấu giá
- bid_price : giá đấu
- bid_time : thời gian thực hiện lượt đấu giá
- auto_bid : true/false. kiểm tra có phải lượt đấu giá là đấu giá tự động không. 

## Lượt đấu giá lần đầu hợp lệ
- Người dùng đang đăng nhập hệ thống
- Người dùng phải có tài khoản Bảo Kim
- Số dư tài khoản Bảo Kim >= số tiền cần thiết để đặt cọc khi đấu giá sản phẩm 
- Thời điểm thực hiện lượt đấu giá diễn ra trước khi phiên đấu giá kết thúc
- Giá đấu >= giá dẫn đầu hiện tại + bước giá

## Lượt đấu giá hợp lệ 
- Người dùng đang đăng nhập hệ thống 
- Thời điểm thực hiện lượt đấu giá diễn ra trước khi phiên đấu giá kết thúc
- Giá đấu >= giá dẫn đầu hiện tại + bước giá