buy_item.mdown
=====================

File này mô tả quy trình mua một sản phẩm

## Quy trình
1. Vào trang chi tiết sản phẩm
2. Chọn số lượng sản phẩm cần mua
3. Ấn nút submit
4. Đăng nhập Vatgia ID(nếu cần)
5. Yêu cầu cập nhật thông tin địa điểm vận chuyển hàng tại tài khoản vật giá (nếu cần)
5. Chuyển sang trang thanh toán Bảo kim 
6. Xác nhận chuyển tiền
7. Trở lại trang xem chi tiết sản phẩm
8. Trang sản phẩm cập nhật status. Người dùng đã đặt mua sản phẩm này.
